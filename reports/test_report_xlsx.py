from odoo import models

class TestReportXLSX(models.AbstractModel):
    _name = 'report.library_management_odoo.report_test_xls'
    _inherit = 'report.report_xlsx.abstract'

    def generate_xlsx_report(self, workbook, data, partners):
        sheet = workbook.add_worksheet('Borrow Book count card')
        heading = workbook.add_format({'font_size': 14, 'align': 'center', 'bold': True})
        p_text = workbook.add_format({'font_size': 14, 'align': 'center'})
        # print("self", self)
        # print("data", datag['from_date'])
        records_obj = self.env['library.records'].search([])
        try:
            print("form date", data['from_date'], "to date",data['to_date'])
            result = self.env['library.records'].search(['&', ('rented_date','>=',data['from_date']), ('rented_date','<=', data['to_date'])])
            result_dates = result.mapped('rented_date')
            print(sorted(result_dates))
        except:
            result_dates = records_obj.mapped('rented_date')

        # records_obj = self.env['library.records'].search([])
        # print("result", result)
        #print(records_obj)
        dates = result_dates

        sheet.write(0, 0, "Date", heading)
        sheet.write(0, 1, "Rented book count", heading)
        for ind,date in enumerate(sorted(set(dates))):
            count = records_obj.search_count([('rented_date','=',date)])
            print(date, 'count: ', count)
            sheet.write(ind+1, 0, date, p_text)
            sheet.write(ind+1, 1, count, p_text)
        # print(dates)


class RecordUserReportXLSX(models.AbstractModel):
    _name = 'report.library_management_odoo.report_record_user_xls'
    _inherit = 'report.report_xlsx.abstract'

    def generate_xlsx_report(self, workbook, data, partners):
        sheet = workbook.add_worksheet('User card')
        heading = workbook.add_format({'font_size': 14, 'align': 'center', 'bold': True})
        p_text = workbook.add_format({'font_size': 14, 'align': 'center'})

        records_obj = self.env['library.records'].search([])
        try:
            print("form date", data['from_date'], "to date",data['to_date'])
            result = self.env['library.records'].search(['&', ('rented_date','>=',data['from_date']), ('rented_date','<=', data['to_date'])])
        except:
            result = records_obj

        sheet.write(0, 0, "Member Id", heading)
        sheet.write(0, 1, "Member Name", heading)
        sheet.write(0, 2, "Book Id", heading)
        sheet.write(0, 3, "Book Name", heading)
        sheet.write(0, 4, "Borrow date", heading)
        sheet.write(0, 5, "Return Date", heading)

        for ind, record in enumerate(result):
            print(record.member_id, record.member_id.name, record.book_id, record.book_id.name, record.rented_date, record.return_date)
            sheet.write(ind + 1, 0, record.member_id.id, p_text)
            sheet.write(ind + 1, 1, record.member_id.name, p_text)
            sheet.write(ind + 1, 2, record.book_id.id, p_text)
            sheet.write(ind + 1, 3, record.book_id.name, p_text)
            sheet.write(ind + 1, 4, record.rented_date, p_text)
            sheet.write(ind + 1, 5, record.return_date, p_text)