from odoo import fields, models,api

class Genre(models.Model):
    _name = 'library.genre'

    name = fields.Char(string='Genre', required=True)
    manager_id = fields.Many2one(string='Manager', comodel_name='res.users', required=True)

    def action_print_check(self):
        print("userID",self.env.user.id)
        print("Self", self.manager_id)

