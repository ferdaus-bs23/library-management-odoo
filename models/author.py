from odoo import fields, models

class Author(models.Model):
    _name = "library.books.author"
    _description = "Books Author Model"

    name = fields.Char('Name',required=True)
    bio = fields.Text('Bio')
    book_id = fields.Many2many(comodel_name="library.books")
