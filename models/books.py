from odoo import fields, models, api
from odoo.exceptions import UserError
from dateutil.relativedelta import relativedelta

class Books(models.Model):
    _name = "library.books"
    _description = "Books Model"

    name = fields.Char('Title',required=True)
    description = fields.Text('Description')
    isbn = fields.Char('ISBN',required=True)
    publication = fields.Char('Publication', required=True)
    author_id = fields.Many2many(string='Author', comodel_name="library.books.author")
    # property_tag_id = fields.Many2many(comodel_name="estate.property.tag", string="Tag")
    rent_available = fields.Boolean("Available", default=True)
    # rented_by_id = fields.Many2one(string='Last Rented by',comodel_name='library.member',copy=False)
    no_books = fields.Integer('Number book',default=1, required=True)
    no_books_available = fields.Integer('Available Amount', compute="_compute_rentable_books_count")
    record_ids = fields.One2many(comodel_name="library.records", inverse_name="book_id")
    genre_id = fields.Many2many(string='Genre', comodel_name="library.genre")

    _sql_constraints = [
        ('check_no_books', 'CHECK(no_books > 0)',
         'Number of books must be positive'),
    ]

    @api.depends("record_ids","no_books")
    def _compute_rentable_books_count(self):
        for record in self:
            # record.no_books_available = record.no_books - len(record.record_ids.mapped('id'))
            rented_records = record.record_ids.filtered(lambda r: r.returned != True).mapped('id')
            # print("Rented",rented_records)
            number_books_available = record.no_books - len(rented_records)
            record.no_books_available = number_books_available

    @api.multi
    def unlink(self):
        for record in self:
            if not record.no_books != record.no_books_available:
                raise UserError("Can not delete a rented book")
        return super().unlink()

    def action_return_check(self):
        print(self.genre_id)


    # def action_borrow(self):
    #     self.ensure_one()
    #     if self.rent_available:
    #         self.rent_available = False
    #         # print(self.env.user.name)
    #         if self.env['library.member'].search([('name','=',self.env.user.name)]):
    #             # print(self.env['library.member'].search([('name','=',self.env.user.name)]).id)
    #             self.rented_by_id = self.env['library.member'].search([('name','=',self.env.user.name)]).id
    #
    #             self.env['library.records'].create({
    #                 'book_id': self.id,
    #                 'member_id': self.env['library.member'].search([('name','=',self.env.user.name)]).id,
    #                 'rented_date': fields.Date.today(),
    #             })
    #         else:
    #             raise UserError('Not a member can not rent')
    #     else:
    #         raise UserError('Not avaiable for rent')
    #     return True
    #
    # def action_return(self):
    #     self.ensure_one()
    #     if not self.rent_available:
    #         if self.env['library.member'].search([('name','=',self.env.user.name)]):
    #             #print('ok 1', self.rented_by_id, self.env['library.member'].search([('name','=',self.env.user.name)]).id)
    #
    #             if self.rented_by_id.id == self.env['library.member'].search([('name','=',self.env.user.name)]).id:
    #                 print('ok 2')
    #                 #self.rent_available = True
    #                 #record = self.env['library.records'].search([('id','=', self.record_ids[-1].id)])
    #                 record = self.env['library.records'].search([('id','=', self.record_ids[-1].id)])
    #                 #print(record)
    #                 #print("self record ids",self.record_ids[-1].id)
    #                 record.return_date = fields.Date.from_string(fields.Date.today()) + relativedelta(weeks=+4)
    #                 #self.rented_by_id = 0
    #                 self.rent_available = True
    #
    #         else:
    #             raise UserError('Not a member, can not return book')
    #         return True