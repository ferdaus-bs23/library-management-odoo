from odoo import fields, models,api
from dateutil.relativedelta import relativedelta
from odoo.exceptions import UserError

class Records(models.Model):
    _name = 'library.records'

    book_id = fields.Many2one(string='Book', comodel_name="library.books", required=True)
    member_id = fields.Many2one(string='Rented by',comodel_name='res.partner', required=True)

    rented_date = fields.Date(string="Borrow date", default=fields.Date.today(), copy=False, required=True)
    return_date = fields.Date(string="Return date", copy=False)
    returned = fields.Boolean(string="Returned?", default=False)
    late_fine = fields.Float(string="Late fine", compute="_compute_return_date")


    _order = "returned, rented_date desc"


    @api.depends("rented_date","return_date")
    def _compute_return_date(self):
        for record in self:
            if record.return_date:
                num_days = int((fields.Date.from_string(record.return_date) - fields.Date.from_string(record.rented_date)).days)
                if num_days > 14:
                    record.late_fine = (num_days -14) * 20.00
                else:
                    #print("less Than 14")
                    record.late_fine = 0.00
            else:
                #print("Not returned")
                record.late_fine = 0.00




    @api.model
    def create(self, vals):
        #print(vals)
        books_obj = self.env['library.books'].browse(vals['book_id'])

        #print(books_obj.no_books_available)
        if books_obj.no_books_available < 1:
            raise UserError("No books avaiable")
        elif books_obj.no_books_available == 1:
            print(books_obj.rent_available)
            books_obj.rent_available = False
            print("After change", books_obj.rent_available)
        return super().create(vals)


    def action_return(self):
        self.ensure_one()
        # print(self)
        rec_book_id = self.book_id
        print(rec_book_id.no_books_available)
        if not rec_book_id.rent_available:
            rec_book_id.rent_available = True
        # book_obj = self.env['library.books']
        # print(book_obj)
        self.return_date = fields.Date.today()
        self.returned = True
        return True


    @api.multi
    def unlink(self):
        for record in self:
            if not record.returned:
                raise UserError("Can not delete the record, book not returned yet")
        return super().unlink()
















