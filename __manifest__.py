# -*- coding: utf-8 -*-

{
    'name': 'Library management',
    'depends': [
        'base_setup', 'report_xlsx',
    ],
    'data': [
        'security/ir.model.access.csv',
        'security/security.xml',
        'wizard/generate_record_report_views.xml',
        'views/library_records_view.xml',
        'views/library_books_view.xml',
        'views/library_books_author_view.xml',
        'views/library_genre_view.xml',
        # 'views/test_view.xml',
        'views/library_member_view.xml',
        'views/library_menu.xml',
        'reports/report.xml',

    ],
    'application': True,
}
