from odoo import fields, models,api

class RecordGenerate(models.TransientModel):
    _name = "record.report.generate"

    from_date = fields.Date(string="From", required=True)
    to_date = fields.Date(string="To", required=True)

    def generate_excel_report(self):
        # print(self)
        #print(self.env.ref('library_management_odoo.report_test_library_xlsx'))
        # return self.env.ref('library_management_odoo.report_test_library_xlsx').report_action(self)

        context = {"from_date": self.from_date, "to_date": self.to_date}
        print(context)
        return self.env.ref('library_management_odoo.report_test_library_xlsx').report_action(self, context)

    def generate_user_excel_report(self):
        context = {"from_date": self.from_date, "to_date": self.to_date}
        print(context)
        return self.env.ref('library_management_odoo.report_user_record_library_xlsx').report_action(self, context)
